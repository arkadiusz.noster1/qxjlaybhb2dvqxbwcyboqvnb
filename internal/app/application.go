package app

import (
	"fmt"
	"gitlab.com/arkadiusz.noster1/qxjlaybhb2dvqxbwcyboqvnb/pkg/validate"
	"log"
	"net/http"
	"time"
)

type App struct {
	mux    *http.ServeMux
	server *http.Server
}

type Option func(app *App) error

func WithPort(port string) Option {
	return func(app *App) error {
		validatedPort, err := validate.Port(port)
		if err != nil {
			return err
		}
		app.server.Addr = validatedPort
		return nil
	}
}

func WithHandler(pattern string, handler http.Handler) Option {
	return func(app *App) error {
		//todo validate pattern
		app.mux.Handle(pattern, handler)
		return nil
	}
}

func NewApp(opts ...Option) (*App, error) {
	mux := http.NewServeMux()
	app := &App{
		mux: mux,
		server: &http.Server{
			Addr:         ":8080",
			Handler:      mux,
			ReadTimeout:  10 * time.Second,
			WriteTimeout: 10 * time.Second,
		},
	}
	for _, opt := range opts {
		if err := opt(app); err != nil {
			return nil, fmt.Errorf("could not create new aplication: %w", err)
		}
	}
	return app, nil
}

func (a App) Run() {
	log.Fatal(a.server.ListenAndServe())
}
