package service

import (
	"context"
	"fmt"
	"gitlab.com/arkadiusz.noster1/qxjlaybhb2dvqxbwcyboqvnb/internal/pictures"
	"time"
)

type ZippedPictures struct {
	imgUrlsService pictures.UrlsService
	imgRepository  pictures.ImagesRepository
	zipper         pictures.Zipper
}

func NewZippedPicturesService(
	imgUrlsService pictures.UrlsService,
	imgRepository pictures.ImagesRepository,
	zipper pictures.Zipper) *ZippedPictures {
	return &ZippedPictures{
		imgUrlsService: imgUrlsService,
		imgRepository:  imgRepository,
		zipper:         zipper,
	}
}

func (z ZippedPictures) GetZipped(ctx context.Context, start time.Time, end time.Time) (pictures.ZippedFilePath, error) {
	urlsChan, err := z.imgUrlsService.GetImagesUrls(ctx, start, end)
	if err != nil {
		return "", fmt.Errorf("Could not get image urls: %w", err)
	}
	imgPaths, err := z.imgRepository.GetImages(ctx, urlsChan)
	if err != nil {
		return "", fmt.Errorf("could not get images: %w")
	}
	zipped, err := z.zipper.Zip(ctx, imgPaths)
	if err != nil {
		return "", fmt.Errorf("Could not zip files from channel: %w", err)
	}
	return zipped, nil
}
