package service

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/arkadiusz.noster1/qxjlaybhb2dvqxbwcyboqvnb/internal/pictures"
	"net/http"
	"net/url"
	"time"
)

func parseDateParam(values url.Values, paramName string) (time.Time, error) {
	paramStr, ok := values[paramName]
	if !ok {
		return time.Time{}, fmt.Errorf("%v=DD-MM-YYYY is required request paremeter", paramName)
	}
	date, err := parseDate(paramStr[0])
	if err != nil {
		return time.Time{}, fmt.Errorf("Wrong %v format: %w", err)
	}
	return date, nil
}

func parseDate(v string) (time.Time, error) {
	return time.Parse("2006-01-02", v)
}

func parseParams(values url.Values) (start time.Time, end time.Time, err error) {
	start, err = parseDateParam(values, "start_date")
	if err != nil {
		return time.Time{}, time.Time{}, err
	}
	end, err = parseDateParam(values, "end_date")
	if err != nil {
		return time.Time{}, time.Time{}, err
	}
	return start, end, nil
}

func NewPicturesHandle(zippedPicsService *ZippedPictures) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		start, end, err := parseParams(r.URL.Query())
		http.Error(w, err.Error(), http.StatusBadRequest)
		zippedFilePath, err := zippedPicsService.GetZipped(context.Background(), start, end)
		if errors.As(err, &pictures.Dummy) {
			http.Error(w, err.Error(), pictures.MapErrToCode(err))
			return
		}
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/zip")
		w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename='%v--%v.zip'", start, end))
		http.ServeFile(w, r, string(zippedFilePath))
	}
}
