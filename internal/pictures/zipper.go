package pictures

import "context"

type ZippedFilePath string

type Zipper interface {
	Zip(ctx context.Context, imgPaths []ImageFilePath) (ZippedFilePath, error)
}
