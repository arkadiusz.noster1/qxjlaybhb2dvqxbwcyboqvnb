package pictures

import (
	"context"
	"net/url"
	"time"
)

type ImageUrl url.URL

type UrlsService interface {
	GetImagesUrls(ctx context.Context, start time.Time, end time.Time) (chan<- ImageUrl, error)
}
