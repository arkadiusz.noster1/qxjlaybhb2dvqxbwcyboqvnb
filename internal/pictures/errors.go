package pictures

import (
	"errors"
	"net/http"
)

type Err error

var (
	Dummy Err = errors.New("dummy err")
)

func MapErrToCode(pictureErr Err) int {
	switch pictureErr {

	default:
		return http.StatusInternalServerError
	}
}
