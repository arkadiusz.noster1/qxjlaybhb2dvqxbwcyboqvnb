package pictures

import "context"

type ImageFilePath string

type ImagesRepository interface {
	GetImages(ctx context.Context, imageUrls chan<- ImageUrl) ([]ImageFilePath, error)
}
