package main

import (
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/arkadiusz.noster1/qxjlaybhb2dvqxbwcyboqvnb/internal/app"
	"gitlab.com/arkadiusz.noster1/qxjlaybhb2dvqxbwcyboqvnb/internal/client"
	"gitlab.com/arkadiusz.noster1/qxjlaybhb2dvqxbwcyboqvnb/internal/nasa"
	"gitlab.com/arkadiusz.noster1/qxjlaybhb2dvqxbwcyboqvnb/internal/service"
	"gitlab.com/arkadiusz.noster1/qxjlaybhb2dvqxbwcyboqvnb/internal/zip"
	"log"
)

type Config struct {
	ApiKey            string `default:"DEMO_KEY"`
	ApiWorkersNumber  int    `default:"5"`
	FileWorkersNumber int    `default:"5"`
	ServerPort        string `default:":8080"`
	TmpStoragePath    string `default:"/tmp"`
}

func main() {
	cfg := Config{}
	envconfig.Process("", &cfg)

	//todo use constructors with validators
	zps := service.NewZippedPicturesService(
		&nasa.UrlGetter{},
		&client.FilesFromUrl{},
		&zip.FromPaths{})

	application, err := app.NewApp(
		app.WithHandler("pictures", service.NewPicturesHandle(zps)),
		app.WithPort(cfg.ServerPort))
	if err != nil {
		log.Fatal(err)
	}
	application.Run()

}
