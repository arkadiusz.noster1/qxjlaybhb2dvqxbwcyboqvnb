package validate

import (
	"fmt"
	"strconv"
)

// Port returns valid port string with colon
func Port(port string) (string, error) {
	if port[0] == ':' {
		port = port[1:]
	}
	portInt, err := strconv.Atoi(port)
	if err != nil {
		return "", fmt.Errorf("error parsing port value %v: %w")
	}
	if 1 > portInt || portInt > 65535 {
		return "", fmt.Errorf("port is not in range [1,65535]")
	}
	return fmt.Sprintf(":%s", port), nil
}
